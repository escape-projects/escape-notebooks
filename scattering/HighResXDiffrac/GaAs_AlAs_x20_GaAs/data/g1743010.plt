# Use this file with GnuPlot to plot the data.
# ATTENTION: GnuPlot v.5 or later is required!
# GnuPlot home is: http://gnuplot.sourceforge.net
set terminal png enhanced small size 448,336 background "#000000"
set output "g1743010.png"
set border 15 lc rgb "#00ff00"
set tics out
set grid lc rgb "#a1a100"
unset key
set style data lines
set format y "%g"
set xlabel "Incidence angle [degr]"
set ylabel "Reflectivity"
set logscale y
set yrange [1e-08 :    1]
plot "g1743010.dat" using 1:2 lc rgb "#ff0000"
