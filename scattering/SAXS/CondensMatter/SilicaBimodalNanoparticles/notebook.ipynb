{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import escape as esc\n",
    "esc.require('0.9.7')\n",
    "import numpy as np\n",
    "from escape.utils.widgets import show\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SAXS. Bimodal silica particles\n",
    "\n",
    "In this notebook we demonstrate modelling of small angle scattering data from bimodal silica particles. \n",
    "More details as well as experimental data you can find in the \n",
    "original paper from Applied Crystallography Journal\n",
    "\n",
    "http://journals.iucr.org/j/issues/2015/05/00/vg5026/\n",
    "\n",
    "This notebook is a demonstration of the core functionality of ESCAPE package.\n",
    "\n",
    "\n",
    "The investigated population of silica nanoparticles consists of particles of two sizes: small particles of type \"A\" and large particles of type \"B\", each with its own Gaussian distribution of sizes with characteristic mean radii $R_A$, $R_B$ and their distribution widths $\\sigma_A$ and $\\sigma_B$.\n",
    "\n",
    "The resulted intensity is calculated as a sum of the scattering intensity contributed by each scatterer, i.e. each assembly of particles as following\n",
    "\n",
    "$I_{mod}(q)=\\sum_c^N\\left[\\int_a^b P(q, x)f(x)dx\\right]S(q)$\n",
    "\n",
    "Each contribution $c$ consists of a form-factor $P(q, x)$ determining the shape of a scatterer, $f(x)$ - distribution of certain parameter of the form-factor\n",
    "in the case of disperse aspects. $S(q)$ is a structure factor for each contribution, which is not relevant for our case and is equal unity.\n",
    "\n",
    "We start implementation of our model with definition of model parameters. In the comments below we give a short description of each parameter.\n",
    "For the details please have a look on the original publication.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Mean Radiuses of silica particles\n",
    "R0A = esc.par(\"Mean_Radius0A\", 8.52224, units=\"nm\")\n",
    "R0B = esc.par(\"Mean_Radius0B\", 37.6503, units=\"nm\")\n",
    "\n",
    "#We still need a normal radius parameter to perform integration of intensity\n",
    "RA = esc.par(\"RadiusA\", 8.52224, units=\"nm\")\n",
    "RB = esc.par(\"RadiusB\", 37.6503, units=\"nm\")\n",
    "\n",
    "#widths of radii distributions\n",
    "s_RA = esc.par(\"Sigma_RA\", 2.00668)\n",
    "s_RB = esc.par(\"Sigma_RB\", 8.29554)\n",
    "\n",
    "#contrast with water\n",
    "eta = esc.par(\"Contrast\", 1.017, scale=1e11, \n",
    "              fixed=True, units = \"cm^-2\")\n",
    "\n",
    "#Represent number concentration of particles\n",
    "NA = esc.par(\"NA\", 1.02979, scale=1e-27)\n",
    "NB = esc.par(\"NB\", 6.5107, scale=1e-31)\n",
    "\n",
    "#Background\n",
    "B = esc.par(\"Background\", 0, userlim=[0, 1], fixed=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can implement form-factors for our model. These are standard form-factors for homegeneous particles with spherical shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#definition of functors for intensities\n",
    "q = esc.var(\"q\")\n",
    "\n",
    "PA = esc.pow(4.0/3.0 * np.pi * esc.pow(RA, 3) * eta * \n",
    "                (3 * (esc.sin(q*RA)-q*RA*esc.cos(q*RA))/esc.pow(q*RA, 3.0)), 2)\n",
    "\n",
    "PB = esc.pow(4.0/3.0 * np.pi * esc.pow(RB, 3) * eta * \n",
    "                (3 * (esc.sin(q*RB)-q*RB*esc.cos(q*RB))/esc.pow(q*RB, 3.0)), 2)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$P_A$ and $P_B$ are the form-factors for the corresponding scatterers without taking into account the size distribution. Since ESCAPE operates mostly with simple functor-type objects, the user has the ability to look at and analyze the behavior of each object, before the final simulation. For example, to visually estimate the contribution of any part of the model or locate a calculation error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qrange = np.linspace(0.0001, 5, 1000)\n",
    "show(PA, coordinates=qrange, ylog=True, xlog=True, xlabel=\"Q, 1/nm\", ylabel=\"PA\")\n",
    "show(PB, coordinates=qrange, ylog=True, xlog=True, xlabel=\"Q, 1/nm\", ylabel=\"PB\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After implementing of form-factors and checking their curves we are ready to finalize our model and to implement integration over size distribution.\n",
    "Here we had to reformulate the expressions, because in ESCAPE there is no Gaussian number-weighted size distribution. For that purposes we introduced expressions for $C_A$ and $C_B$ normalization constants which we will use as multipliers for the form-factor expressions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#ESCAPE Gaussian distribution requires FWHM \n",
    "fwhm_RA = s_RA*2.355\n",
    "fwhm_RB = s_RB*2.355\n",
    "\n",
    "#Normalization constant for number size Gaussian distribution\n",
    "CA = 2.0*NA/(1.0+esc.erf(R0A/(np.sqrt(2.0)*s_RA)))\n",
    "CB = 2.0*NB/(1.0+esc.erf(R0B/(np.sqrt(2.0)*s_RB)))\n",
    "\n",
    "#convolution over size distribution\n",
    "PAr = esc.average_normal(CA*PA, fwhm_RA, RA, R0A, epsabs=1e-8, epsrel=1e-8, maxiter=30, numpoints=61, numstd=5)\n",
    "PBr = esc.average_normal(CB*PB, fwhm_RB, RB, R0B, epsabs=1e-8, epsrel=1e-8, maxiter=30, numpoints=61, numstd=5)\n",
    "\n",
    "#Intensity plus background\n",
    "I = PAr + PBr + B\n",
    "\n",
    "show(I, coordinates=qrange, ylog=True, xlog=False, xlabel=\"Q, 1/nm\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are ready now to perform fitting of experimental data. To do that we need to open the data file, which was taken from the supplementary material to the original article. To open the file we will use the *numpy* library and the *loadtxt* function. The data will be loaded into the arrays x, y, err. Then we create a data object Escape *dobj*. The next step is to create a model. Here everything is simple, we will use the function *model*. The arguments *residuals_scale* and *weight_type* are relevant to the calculation of the cost function. *Weight_type* set to \"data\" means that the data errors will be used for the weights.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#data\n",
    "x, y, err=np.loadtxt(\"data/SilicaS2929.pdh\", unpack=True)\n",
    "dobj = esc.data(\"SilicaS2929.pdh\", x, y, err, copy=True)\n",
    "\n",
    "#model\n",
    "mobj = esc.model(\"Model\", I, dobj, residuals_scale=\"none\", weight_type=\"data\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's a good time to create and run an optimizer. We will start by testing the well-known Levenberg-Marquardt optimizer by shaking out the initial parameter values beforehand.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = esc.levmar(\"LM\", mobj, maxiter=100, xtol=1e-15, ftol=1e-15, gtol=1e-15, nupdate=1)\n",
    "opt.shake()\n",
    "opt()\n",
    "show(opt, ylog=True, xlog=True, xlabel=\"Q, 1/nm\", ylabel=\"Intensity\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The values of the parameters and their errors are very close to the values presented in the article, although there is some expected discrepancy. We try now Differential Evolution algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Differential evolution fit with final polishing of parameters with LMA optimizer\n",
    "\n",
    "opt = esc.diffevol(\"DiffEvol\", mobj, maxiter=50, polish_final_maxiter=50, \n",
    "                   polish_candidate_maxiter=0, minconv=1e-5, nupdate=1)\n",
    "opt.shake()\n",
    "opt()\n",
    "show(opt, ylog=True, xlabel=\"Q, 1/nm\", ylabel=\"Intensity\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results of LM and DE optimizers for the presented data are in a good agreement with results presented in the original paper. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
