{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import escape as esc\n",
    "from escape.utils.widgets import show\n",
    "import numpy as np\n",
    "esc.require('0.9.7')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical integration\n",
    "\n",
    "Integration in scattering is used quite often. A good example is an averaging of a form-factor over certain parameter, like size of a particle, in small angle scattering. The best if a solution of this integral can be found analytically, but this is not always possible. Either integral doesn't have analytical solution or its solution is too specific and can be applied for a particular limited cases only, like particular distribution function.\n",
    "\n",
    "Also, analytical calculation could require a lot of time and result usually is needed as soon as possible. Plus, one can use a numerical integration for a proof of a found analytical solution.\n",
    "\n",
    "Here, methods of numerical integration of a functor object are presented. Currently, numerical integration routines in ESCAPE are based on Gauss-Kronrod Quadratures method. Below are few examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standard integration\n",
    "\n",
    "ESCAPE math module supports two types of integration with differential of functor variable or functor parameter.\n",
    "\n",
    "To find a numerical solution of integral of type $\\int_a^bf(x, p)dx$ or $\\int_a^bf(x, p)dp$, where $x$ is a variable of functor $f(x, p)$ and $p$ is a parameter and $a, b$ are the finite limits of integration, one should use *integral* method as following:\n",
    "\n",
    "```python\n",
    "\n",
    "    integral(F, vp, a, b, numpoints=7, epsabs=None,\n",
    "              epsrel=None, maxiter=None)\n",
    "\n",
    "```\n",
    "where $F$ - functor with double return type, $x$ - integration variable or parameter, $a, b$  - integration limits,\n",
    "*numpoints* - number of quadrature points \\[7, 15, 21, 51, 61\\]), *epsabs* and *epsrel* - absolute and relative tolerances for adaptive integration, *maxiter* - maximum number of iterations of adaptive integration. if maxiter is zero, adaptive integration is switched off. If you integrate a functor of one variable $x$ over this variable, the result if *integrate* method is a dependent parameter. if you integrate a functor of two variables over one of them like $\\int F(x, y)dx$, the returned object will be a functor $I(y)$. Below there are several examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X=esc.var(\"X\")\n",
    "Y=esc.var(\"Y\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = esc.par(\"P\", 1.0)\n",
    "F1 = esc.sin(p*X)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we create an integral of f(x; p) - I, which is a parameter  \n",
    "I = esc.integral(F1, X, 0, 1.0)\n",
    "print (type(I))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(I)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#integration of 2d functor\n",
    "F2 = esc.sin(p*X)*esc.cos(p*Y)\n",
    "I=esc.integral(F2, X, 0.0, 1.0, epsabs=1e-4, epsrel=1e-5, maxiter=10)\n",
    "show(I, xlabel=\"Y\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#integration over parameter\n",
    "p = esc.par(\"P\", 1.0)\n",
    "F3 = esc.sin(p*X)\n",
    "I = esc.integral(F3, p, 0, 1.0)\n",
    "show(I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Averaging of a function with respect to distribution function\n",
    "\n",
    "\n",
    "ESCAPE implements several useful well-known distribution functions and their integrals. For the integration we still use Gauss-Kronrod quadratures and there is no support for infinite integration limits, but for typical distribution function we present here, the integration limits either are finite or can be replaced by finite integration limits still preserving high accuracy of the integration result. As for the standard integration routine, there is a support for integration over a functor variable or over a parameter.\n",
    "\n",
    "### Integration over variable\n",
    "\n",
    "Integration over variable can be described by the following equation:\n",
    "\n",
    "$F(x_0)=\\int_a^b f(x)G(x, x_0; \\sigma(x_0))dx$, where $x_0$ - is a variable which correspond to the mean value, $G(x, x_0; \\sigma(x_0))$ - distribution function of two variables $x$ and $x_0$, $\\sigma(x_0)$ - $\\sigma$ or FWHM functor, $a$ and $b$ are the integration limits, which depend on distribution function and its standard deviation.\n",
    "\n",
    "### Integration over parameter\n",
    "\n",
    "Integration over parameter can be described by the following equation:\n",
    "\n",
    "$F(x; p_0)=\\int_a^b f(x; p)G(x, p_0; \\sigma(p_0))dp$, where $p_0$ - is a parameter which correspond to the mean value, $p$ - integration parameter, $G(p, p_0; \\sigma(p_0))$ - distribution functor of one variable and $p_0$ parameter as a mean value, $\\sigma(p_0)$ - $\\sigma$ or FWHM parameter.\n",
    "\n",
    "As an example we integrate a function $f(x)=sin^2(px)$ over the variable $x$ and over the parameter $p$. The parameter $p$ defines the oscillation frequency of the integrated functor. At high oscillation frequencies, non-adaptive Gauss-Kronrod method can fail. That's why in all examples below we have set the adaptive integration method with quite high *maxiter=100* parameter, just be sure that adaptive integration has large enough maximum number of iterations and will converge to the proper solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X0=esc.var(\"X0\")\n",
    "\n",
    "#Parameter 'p' and its mean value 'p0'\n",
    "p=esc.par(\"p\", 5.0, userlim=[0, 10])\n",
    "#p0 is used only for integration over parameter\n",
    "p0=esc.par(\"p0\", 5.0, userlim=[0, 10])\n",
    "#Sigma parameter used for gamma and schulz distribution functions\n",
    "sigma=esc.par(\"Sigma\", 0.2)\n",
    "\n",
    "#functor to integrate\n",
    "F=esc.pow(esc.sin(X*p), 2.0)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gamma distribution function\n",
    "\n",
    "We use the following form of the gamma distribution function:\n",
    "\n",
    "$G(x; x_0, \\sigma)=1/\\theta\\exp((k-1)\\log(t)-x/\\theta-\\log(\\Gamma(k)))$\n",
    "\n",
    "where\n",
    "\n",
    "$\\theta = \\sigma^2 x_0$\n",
    "\n",
    "$k = x_0 / \\theta$\n",
    "\n",
    "$t = x / \\theta$\n",
    "\n",
    "   \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.gamma(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"Gamma distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 10 iterations and 7 standard deviations for the limits\n",
    "I = esc.average_gamma(F, sigma, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_gamma(F, sigma, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Schulz-Zimm distribution function\n",
    "\n",
    "    \n",
    "$G(x; x_0, \\sigma)=t / x \\exp((k - 1) \\log(t) - t - \\log(\\Gamma(k)))$\n",
    "\n",
    "where\n",
    "\n",
    "$k = 1/\\sigma^2$\n",
    "\n",
    "$t = kx/x_0$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.schulz(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"Schulz-Zimm distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 100 iterations and 7 standard deviations for the limits\n",
    "I = esc.average_schulz(F, sigma, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_schulz(F, sigma, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### LogNorm distribution function\n",
    "\n",
    "\n",
    "    \n",
    "$G(x; x_0, \\sigma)=\\exp(\\log^2(x / x_0) / 2 / \\sigma^2))/(\\sqrt{2\\pi}\\sigma x)$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.lognorm(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"LogNorm distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 10 iterations and 7 standard deviations for the limits\n",
    "I = esc.average_lognorm(F, sigma, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_lognorm(F, sigma, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Normal distribution function\n",
    "    \n",
    "$G(x; x_0, \\sigma)=\\frac{1}{\\sqrt{2\\pi}\\sigma}e^{(-(x - x_0)^2 / (2 \\sigma^2))}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.normal(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"Normal distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 10 iterations and 7 standard deviations for the limits\n",
    "I = esc.average_normal(F, sigma*2.355, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_normal(F, sigma*2.355, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8, numstd=10)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Triangular distribution function\n",
    "    \n",
    "    \n",
    "$G(x; x_0, \\sigma) = 2 (x - a) / (b - a) / (x_0 - a) $,\n",
    "\n",
    "for $a<=x<=x_0$\n",
    "\n",
    "$G(x; x_0, \\sigma) = 2 (b - x) / (b - a) / (b - x_0) $,\n",
    "\n",
    "for $x_0<=x<=b$,\n",
    "\n",
    "where $a=x_0-\\sigma$ and $b=x_0+\\sigma$\n",
    "\t\t\t"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.triangular(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"Triangular distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 10 iterations and 7 standard deviations for the limits\n",
    "I = esc.average_triangular(F, sigma*2, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_triangular(F, sigma*2, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Uniform distribution function\n",
    "\n",
    "if $w$ is a FWHM.\n",
    "\n",
    "\n",
    "$G(x; x_0, \\sigma) = 1.0 / w $,\n",
    "\n",
    "for $x_0-w/2<=x<=x_0+w/2$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# coordinates array\n",
    "x=np.arange(0, 10, 0.01)\n",
    "\n",
    "G=esc.uniform(\"G\", X, p0, sigma)\n",
    "show(G, coordinates=x, title=\"Uniform distribution\")\n",
    "\n",
    "#integration over variable\n",
    "show(F, coordinates=x, title=\"F(x)=sin(px)^2\")\n",
    "\n",
    "#we use adaptive integration with max 100 iterations \n",
    "I = esc.average_uniform(F, sigma*2, X, X0, maxiter=100, epsrel=1e-8, epsabs=1e-8)\n",
    "show(I, coordinates=x, title=\"Integration over variable\", xlabel=\"X0\", ylabel=\"I\")\n",
    "\n",
    "#integration over parameter\n",
    "I = esc.average_uniform(F, sigma*2, p, p0, maxiter=100, epsrel=1e-8, epsabs=1e-8)\n",
    "show(I, coordinates=x, title=\"Integration over parameter\", xlabel=\"X\", ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Custom distribution function\n",
    "\n",
    "*Average* method allows one to find an average of any functor with user-defined distribution function. \n",
    "This is how the code for the user-defined Gaussian distribution looks like. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# averaging over variable\n",
    "G=1/np.sqrt(2*np.pi)/sigma*esc.exp(-(X-X0)*(X-X0)/(2*sigma*sigma))\n",
    "G.variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "I=esc.average(F, G, X, X0, X0-10*sigma, X0+10*sigma, maxiter=100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(I, coordinates=np.arange(0, 10, 0.01), xlabel=\"X0\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# averaging over parameter\n",
    "G=1/np.sqrt(2*np.pi)/sigma*esc.exp(-(p-p0)*(p-p0)/(2*sigma*sigma))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "I=esc.average(F, G, p, p0, p0-5*sigma, p0+5*sigma, maxiter=100)\n",
    "show(I, ylabel=\"I\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
